package com.me.cookingAPI.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;

@Configuration
public class Config extends AbstractCouchbaseConfiguration{

	@Override
	public String getConnectionString() {
		return "couchbase://127.0.0.1";
	}

	@Override
	public String getUserName() {
		return "c1te";
	}

	@Override
	public String getPassword() {
		return "administrator";
	}

	@Override
	public String getBucketName() {
		return "temp";
	}

	
}
