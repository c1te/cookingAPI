package com.me.cookingAPI.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.me.cookingAPI.entity.Trainer;
import com.me.cookingAPI.repo.TrainerRepo;

@RestController
public class TrainerController {

	@Autowired
	private TrainerRepo trainerRepo; 
	
	@RequestMapping("/")
    public String index() {
        return "Welcome to the CRUD application!!";
    }
	
	@PostMapping("/newTrainer")
	public Trainer addTrainer(@RequestBody Trainer newTrainer) {
		return trainerRepo.save(newTrainer);
	}
	
	@GetMapping("/trainer/{id}")
	public Optional<Trainer> getTrainer(@PathVariable String id){
		if(trainerRepo.existsById(id)){
			return trainerRepo.findById(id);
		}
		else {
			return Optional.empty();
		}
	}
	
	@DeleteMapping("/trainer/{id}")
	public void deleteTrainer(@PathVariable String id) {
		trainerRepo.deleteById(id);
	}
	
	@PutMapping("/trainer/{id}")
	public Trainer updateTrainer(@PathVariable String id,@RequestBody Trainer updatedTrainer) {

		Trainer existingTrainer = trainerRepo.findById(id).orElse(null);
        if (existingTrainer != null) {
            // Update fields with new values
            existingTrainer.setName(updatedTrainer.getName());
            existingTrainer.setEmail(updatedTrainer.getEmail());
            existingTrainer.setAge(updatedTrainer.getAge());

            // Save the updated product
            return trainerRepo.save(existingTrainer);
        } else {
            throw new IllegalArgumentException("Trainer not found with ID: " + id);
        }
		
	}
}
