package com.me.cookingAPI.repo;

import org.springframework.data.couchbase.repository.CouchbaseRepository;

import com.me.cookingAPI.entity.Trainer;

public interface TrainerRepo extends CouchbaseRepository<Trainer,String> {

}
