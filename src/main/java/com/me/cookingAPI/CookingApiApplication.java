package com.me.cookingAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CookingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CookingApiApplication.class, args);
	}

}
